# Qt Examples

Various little examples to help you start developing with Qt.  
Feel free to contribute to this repo with more examples, widgets, layouts, configuration and so on.

## Counter
Counting a number up and down with button presses.  
Uses `QPushButton` and `QLineEdit`.  
Special here is that `myLineEdit` is an extended `QLineEdit` with a new `slot` (callback function).

## Text2Console
Print the content of a text field to std::cout on button press.  
Uses `QPushButton` and `QLineEdit`.  
Special here is the division of functionality between view MainWindow and control Control.  
It is not shown here, but when using a *model* it doesn't have to be a `QObject` and can thus be fully independend from the Qt Framework.

## ModelViewList
Add and remove Items inside a model and display them in the view.  
Uses `QListWidget`, `QListWidgetItem`, `QPushButton` and `QLineEdit`.  
Special here is the representation of a model list in a different view list as well their modification.
It is also shown how an own subclass of `QListWidgetItem` could be used instead of the original.
Different `slots` are defined, as well as one `signal`, although they don't use parameters.



# Building

Should work out of the box with Qt Creator, VSCode and the CMake and C++ plugins installed.  

    [cmake] CMake Error: CMake was unable to find a build program corresponding to "Unix Makefiles".  CMAKE_MAKE_PROGRAM is not set.  You probably need to select a different build tool.

    #Manjaro:
    sudo pacman -S base-devel

    #Ubuntu:
    sudo apt install build-essential