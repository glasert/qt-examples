#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <iterator> 
#include <string>
#include <QObject>

class Model : public QObject
{
    Q_OBJECT

private:
    std::vector<std::string> vec;

public:
    std::vector<std::string> getVec() { return vec; }

    void addNewItem(std::string);
    void removeItem(uint);

signals:
    void listChanged();
};

#endif //MODEL_H