#include "model.h"


void Model::addNewItem(std::string s)
{
    vec.emplace_back(s);
    emit(listChanged());
}

void Model::removeItem(uint index)
{
    if (index < vec.size())
    {
        std::vector<std::string>::iterator it = vec.begin();
        std::advance(it, index);
        vec.erase(it);
        emit(listChanged());
    }
}