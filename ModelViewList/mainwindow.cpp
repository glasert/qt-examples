#include <QBoxLayout>
#include <QListWidget>
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    //general
    setWindowTitle("ModelViewList");

    // Layout
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::Direction::TopToBottom);
    QWidget *widget = new QWidget;
    widget->setLayout(layout);
    setCentralWidget(widget); // special for MainWindow

    //TextField
    qLineEdit = new QLineEdit();
    layout->addWidget(qLineEdit);

    // Buttons
    qAddButton = new QPushButton("Add");
    layout->addWidget(qAddButton);
    qRemButton = new QPushButton("Remove");
    layout->addWidget(qRemButton);

    // List
    qList = new QListWidget();
    layout->addWidget(qList);
}