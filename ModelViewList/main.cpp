#include <QApplication>

#include "mainwindow.h"
#include "model.h"
#include "control.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Model model;
    MainWindow view;
    Control control(model, view);

    view.show();
    return a.exec();
}