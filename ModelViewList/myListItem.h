#ifndef MYLISTITEM_H
#define MYLISTITEM_H

#include <QListWidgetItem>
#include <string>

class myListItem : public QListWidgetItem
{
public:
    myListItem(std::string &text)
    {
        setText(text.c_str());
    };
};

#endif //MYLISTITEM_H