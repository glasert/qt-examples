#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QPushButton>
#include <QListWidget>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QLineEdit   *qLineEdit;
    QPushButton *qAddButton;
    QPushButton *qRemButton;
    QListWidget *qList;

    MainWindow(QWidget *parent = nullptr);
};

#endif // MAINWINDOW_H
