#ifndef CONTROL_H
#define CONTROL_H

#include <QObject>
#include "model.h"
#include "mainwindow.h"


class Control : public QObject
{
    Q_OBJECT

private:
    MainWindow &view;
    Model &model;


public:
    Control(Model &model, MainWindow &view);

public slots:
    void onAddButton();
    void onRemButton();

    void onListChange();
};

#endif //CONTROL_H