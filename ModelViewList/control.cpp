#include <QListWidgetItem>
#include "control.h"
#include "myListItem.h"

Control::Control(Model &model, MainWindow &view) : model(model), view(view)
{
    connect(view.qAddButton, SIGNAL(clicked()), this, SLOT(onAddButton()));
    connect(view.qLineEdit, SIGNAL(returnPressed()), this, SLOT(onAddButton()));
    connect(view.qRemButton, SIGNAL(clicked()), this, SLOT(onRemButton()));

    connect(&model, SIGNAL(listChanged()), this, SLOT(onListChange()));
}

void Control::onAddButton()
{
    std::string s = view.qLineEdit->text().toStdString();
    view.qLineEdit->clear();
    if (!s.empty())
        model.addNewItem(s);
}

void Control::onRemButton()
{
    uint index = view.qList->currentRow();
    model.removeItem(index);
}

void Control::onListChange()
{
    auto vec = model.getVec();
    view.qList->clear(); // This could use some more precision, but whatever.
    for (auto &&text : vec)
    {
        // Using standard QListWidgetItem
        QListWidgetItem *li = new QListWidgetItem;
        li->setText(text.c_str());

        // Or using our own subclass:
        //myListItem *li = new myListItem(text);

        view.qList->addItem(li);
    }
}