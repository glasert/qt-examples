#include "mainwindow.h"
#include "control.h"

#include <QApplication>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow view;
    Control control(&view);

    view.show();
    return a.exec();
}
