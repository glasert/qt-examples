#include <QBoxLayout>

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
//general
    setWindowTitle("text2Console");

// Layout
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);
    QWidget *widget = new QWidget;
    widget->setLayout(layout);
    this->setCentralWidget(widget); // special for MainWindow

// LineEdit
    lineEdit = new QLineEdit();
    layout->addWidget(lineEdit);

// Button
    this->button = new QPushButton("Print");
    layout->addWidget(button);
}

MainWindow::~MainWindow()
{
}

