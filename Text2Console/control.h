#ifndef CONTROL_H
#define CONTROL_H

#include <QObject>
#include "mainwindow.h"

class Control : public QObject
{
    Q_OBJECT

private:
    MainWindow *view;

public:
    Control(MainWindow *_view);
    ~Control();

private slots:
    void onButton();

};
#endif // CONTROL_H
