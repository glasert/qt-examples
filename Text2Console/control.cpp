#include <iostream>

#include "control.h"
#include <QObject>

Control::Control(MainWindow *_view)
{
    view = _view;
    connect(view->button, SIGNAL(clicked()), this, SLOT(onButton()));
}

void Control::onButton()
{
    QString text = view->lineEdit->text();

    std::string str = text.toStdString();

    std::cout << str << std::endl;
}

Control::~Control()
{
}