#include "myLineEdit.h"

void myLineEdit::updateFromInt(int num)
{
    QString str = QString(std::to_string(num).c_str());
    this->setText(str);
}