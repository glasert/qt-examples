#include <QPushButton>
#include <QLineEdit>
#include <QBoxLayout>

#include "myLineEdit.h"

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
//general
    setWindowTitle("Counter");
// Layout
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::Direction::TopToBottom);
    QWidget *widget = new QWidget;
    widget->setLayout(layout);
    this->setCentralWidget(widget); // special for MainWindow

// Increment Button
    QPushButton *plusButton = new QPushButton("Increase");

    layout->addWidget(plusButton);
    connect(plusButton, SIGNAL(clicked()), this, SLOT(onPlusButton()));

// Derived Display
    myLineEdit *mydisplay = new myLineEdit("0"); //Constructors are inherited
    mydisplay->setReadOnly(true);
    mydisplay->setAlignment(Qt::AlignCenter);
    QFont font1 = mydisplay->font();
    font1.setPointSize(font1.pointSize() + 8);
    mydisplay->setFont(font1);

    layout->addWidget(mydisplay);
    connect(this, SIGNAL(counterChanged(int)), mydisplay, SLOT(updateFromInt(int)));

// Decrement Button
    QPushButton *minusButton = new QPushButton("Decrease");

    layout->addWidget(minusButton);
    connect(minusButton, SIGNAL(clicked()), this, SLOT(onMinusButton()));

// Q Display
    QLineEdit *qdisplay = new QLineEdit("0");
    qdisplay->setReadOnly(true);
    qdisplay->setAlignment(Qt::AlignCenter);
    QFont font2 = qdisplay->font();
    font2.setPointSize(font2.pointSize() + 8);
    qdisplay->setFont(font2);

    layout->addWidget(qdisplay);
    connect(this, SIGNAL(counterChanged(const QString&)), qdisplay, SLOT(setText(const QString&)));
}

void MainWindow::onPlusButton()
{
    ++Counter;

    //For derived display
    emit(counterChanged(Counter));

    //For Q display
    QString str = QString(std::to_string(Counter).c_str());
    QString& strRef = str;
    emit(counterChanged(strRef));
}

void MainWindow::onMinusButton()
{
    --Counter;

    //For derived display
    emit(counterChanged(Counter));

    //For Q display
    QString str = QString(std::to_string(Counter).c_str());
    emit(counterChanged(str));
}

MainWindow::~MainWindow()
{
}

