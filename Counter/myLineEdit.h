#ifndef MYLINEEDIT_H
#define MYLINEEDIT_H


#include <QLineEdit>

class myLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    using QLineEdit::QLineEdit; // inherit constructors of QLineEdit

public slots:
    void updateFromInt(int);
};

#endif // MYLINEEDIT_H