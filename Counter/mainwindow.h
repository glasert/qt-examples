#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    int Counter = 0;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onPlusButton();
    void onMinusButton();

signals:
    void counterChanged(int);
    void counterChanged(const QString &);
};
#endif // MAINWINDOW_H
